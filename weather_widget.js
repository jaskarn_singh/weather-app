/*
 * Constructor function for a WeatherWidget instance.
 * 
 * container_element : a DOM element inside which the widget will place its UI
 *
 */
 
function WeatherWidget(container_element){

	//Data properties of the object 
	
	var _list = [];   //an array of currently downloaded phone listings as PhoneLine objects
	//var _request ;    //the XHR request object
	var _currentSortOrder = 1;    //keep track of how the data is sorted, default is 1 = sort by name
	
	var _ui = {     //an inner object literal representing the widget's UI
		
		sortByTown  :   null,    //a button to sort by name
		sortByTemp	:   null,    // a button to sort by id
		container	:	null,	// the container for all of the UI elements
		selector	:	null,	//div for the selector
		titlebar	:	null,	//div to organise UI elements
		toolbar		: 	null,   //div to organise UI elements
		list		: 	null,  //the div area which will hold the WeatherLine object UIs
		
	};

	 /**
		//Inner object literal to represent the widget's UI
		//function for the DOM elements for the UI
		*/
		var _createUI = function(container){
		//create the container for all of the UI elements and set up the titlebar
		_ui.container = container_element;
		_ui.container.className = "monitor";
		_ui.container.setAttribute("id", container.id + "_container");
		_ui.titlebar = document.createElement("div");
		_ui.titlebar.className = "title";
		_ui.titlebar.label = document.createElement("span");
		_ui.titlebar.label.innerHTML = "Select Town: ";
	//	_ui.titlebar.input = document.createElement("select");
		_ui.titlebar.appendChild(_ui.titlebar.label);
		//_ui.titlebar.appendChild(_ui.titlebar.input);

		//set up for the toolbar
		_ui.toolbar = document.createElement("div");
		_ui.toolbar.label = document.createElement("span");
		_ui.toolbar.label.innerHTML = "Sort By: ";

		//set up for the sort by town radio button
		_ui.sortByTown = document.createElement("input");
		_ui.sortByTown.setAttribute("type", "radio");
		_ui.sortByTown.setAttribute("id", container.id +"town");
		_ui.sortByTown.onclick = function(){
			_doSort(1);
			document.getElementById(container.id +"max").checked = false;
	   };
		_ui.sortByTown.label = document.createElement("span");
		_ui.sortByTown.label.innerHTML = "Town";

		//set up for the sort by max temperture radio button
		_ui.sortByTemp = document.createElement("input");
		_ui.sortByTemp.setAttribute("type", "radio");
		_ui.sortByTemp.setAttribute("id", container.id +"max");
		_ui.sortByTemp.onclick = function(){
			_doSort(0);
			document.getElementById(container.id +"town").checked = false;
	   };
		_ui.sortByTemp.label = document.createElement("span");
		_ui.sortByTemp.label.innerHTML = "Max Temp";

		//main toolbar div which holds all toolbar items
		_ui.toolbar.appendChild(_ui.toolbar.label);
		_ui.toolbar.appendChild(_ui.sortByTown);
		_ui.toolbar.appendChild(_ui.sortByTown.label);
		_ui.toolbar.appendChild(_ui.sortByTemp);
		_ui.toolbar.appendChild(_ui.sortByTemp.label);

		//set up for the selector + select element
		_town1_text = document.createTextNode("Hamilton");
		_town1 = document.createElement("option");
		_town1.setAttribute("value", "Hamilton");
		_town1.appendChild(_town1_text);

		_town2_text = document.createTextNode("Auckland");
		_town2 = document.createElement("option");
		_town2.setAttribute("value", "Auckland");
		_town2.appendChild(_town2_text);

		_town3_text = document.createTextNode("Christchurch");
		_town3 = document.createElement("option");
		_town3.setAttribute("value", "Christchurch");
		_town3.appendChild(_town3_text);

		_town4_text = document.createTextNode("Dunedin");
		_town4 = document.createElement("option");
		_town4.setAttribute("value", "Dunedin");
		_town4.appendChild(_town4_text);

		_town5_text = document.createTextNode("Tauranga");
		_town5 = document.createElement("option");
		_town5.setAttribute("value", "Tauranga");
		_town5.appendChild(_town5_text);

		_town6_text = document.createTextNode("Wellington");
		_town6 = document.createElement("option");
		_town6.setAttribute("value", "Wellington");
		_town6.appendChild(_town6_text);

		_default_text = document.createTextNode("");
		_default = document.createElement("option");
		_default.setAttribute("class", "default");
		_default.appendChild(_default_text);
		
		//main selector div/dropdown which holds all titlebar items
		_ui.titlebar.input = document.createElement("select");
		_ui.titlebar.input.setAttribute("id",container.id + "_selector");
		_ui.titlebar.appendChild(_ui.titlebar.input);
		_ui.titlebar.input.appendChild(_default);
		_ui.titlebar.input.appendChild(_town1);
		_ui.titlebar.input.appendChild(_town2);
		_ui.titlebar.input.appendChild(_town3);
		_ui.titlebar.input.appendChild(_town4);
		_ui.titlebar.input.appendChild(_town5);
		_ui.titlebar.input.appendChild(_town6);
		
		// Div to hold all the Weather items
		_ui.list = document.createElement("div");
		_ui.list.setAttribute("id",container.id + "section");
		_ui.list.className = "list";
		_ui.container.appendChild(_ui.titlebar);
		_ui.container.appendChild(_ui.toolbar);
		_ui.container.appendChild(_ui.list);
		
	// onchange function that sends a xmlhttp request
	_ui.titlebar.input.onchange = function(town)
		{
			//selected variables for onchange
			var id = document.getElementById(container.id +"_selector").selectedIndex;
			var op = document.getElementById(container.id +"_selector").options;
			
			//  for (var i = 1; i < _list.length; i++)
			// 		{
			// 			if(town = _list)
			// 				{
			// 					 	console.log(town, op);
			// 						//op = town;
			// 						alert("Already in List");
			// 				}
								

						//xmlhttp request + JSON / XHR request onject
						var xmlhttp = new XMLHttpRequest();
						{
								xmlhttp.onreadystatechange = function()
									{
									if(this.readyState == 4 && this.status == 200)
										{
										var data = JSON.parse(this.responseText);
										var town = data[0].town;
										var outlook = data[0].outlook;
										var min = data[0].min_temp;
										var max = data[0].max_temp;

										//console.log(town, outlook, min, max);
										
										var witem = new WLine(town, outlook, min, max);
										
										_list.push(witem);
										_refreshTownList();
										}
									};
								xmlhttp.open("GET", "weather.php?town=" + op[id].text, true);
								xmlhttp.send(); 
								
							
						
						}
				
		} 

	}

	//add any other methods required for the functionality
	//function for refreshing the list of towns
	var _refreshTownList = function() {
		//removes all child nodes of the ui.list div
		if(_ui.list == null)
			return;
		while(_ui.list.hasChildNodes()){
			_ui.list.removeChild(_ui.list.lastChild);
		}
		//make sure the data is correctly sorted

	   if(_currentSortOrder == 1){
			_list.sort(_townsort);
		} else {
			_list.sort(_maxsort);
		}
		
		//adds all items back to the UI
		for(var i = 0; i < _list.length; i++){
			var wline = _list[i];
			_ui.list.appendChild(wline.getDomElement());
		}
	}

   
   /**
	*  private method to sort the data - sets the _currentSortParameter and then
	*  calls _refreshTownList where the sorting occurs and display is updated
	*/
	 //sort function
	var _doSort = function(sortBy){
		if(sortBy == 1){
		_currentSortOrder = 1;	 	
		}
		else{
			_currentSortOrder = 0;
		}
			_refreshTownList();
	}
	
	/**
	 *  Comparator functions for sorting townlist items
	 */	  
   
   //max temp sort function
   var _maxsort = function(a,b){
	   return b.getMax() - a.getMax();
   }
   
   //town sort function
   var _townsort = function(a, b){
	   if(a.getTown() > b.getTown())
		   return 1;
	   else if (a.getTown() < b.getTown())
		   return -1;
	   else
		   return 0;
	   }
	 /**
	  * private method to intialise the widget's UI on start up
	  * this method is complete
	  */
	  var _initialise = function(container_element){
	  	_createUI(container_element);
	  	}
	  	
		  
	/*********************************************************

	* Constructor Function for the inner WLine object to hold the 
	* full weather data for a town
	********************************************************/
	
	var WLine = function(town, outlook, min, max){
		
		//data properties for the object

		var _town = town;
		var _outlook = outlook;
		var _min = min;
		var _max = max;

		//Inner object literal to represent the widget's UI
		var _ui = {

			dom_element : null, //dom element for each line of data
			town_label  : null,	// visible data "town"
			outlook_label : null, // visible data "outlook"
			min_label : null,	// visible data "min"
			max_label : null,	// visible data "max"
		};

		//Function for the DOM elements for the UI
		var _createUI = function(container){
		
			//set up for listed objects
			_ui.container = container_element;
			_ui.container.className = "monitor";
			_ui.dom_element = document.createElement("div");
			_ui.dom_element.className = "section";

			_ui.town_label = document.createElement("span");
			_ui.town_label.innerHTML = _town+ "  ";

			_ui.outlook_label = document.createElement("span");
			_ui.outlook_label.innerHTML = _outlook+ "  ";

			_ui.min_label = document.createElement("span");
			_ui.min_label.innerHTML = _min+ "  ";

			_ui.max_label = document.createElement("span");
			_ui.max_label.innerHTML = _max+ "  ";

		//main div that holds dom element items
		_ui.dom_element.appendChild(_ui.town_label);
		_ui.dom_element.appendChild(_ui.outlook_label);
		_ui.dom_element.appendChild(_ui.min_label);
		_ui.dom_element.appendChild(_ui.max_label);
		}
		
		

		//Remaining functions needed for the object

		this.getDomElement = function(){
			return _ui.dom_element;
		}
		this.getTown = function(){
			return _town;
		}
		this.getMax = function(){
			return _max;
		}
		//_createUI() method is called when the object is instantiated
		_createUI();


	 
  	};  //this is the end of the constructor function for the WLine object 
	
	
	//  _initialise method is called when a WeatherWidget object is instantiated
	 _initialise(container_element);
}
	 
//end of constructor function for WeatherWidget 	 
	 
	 